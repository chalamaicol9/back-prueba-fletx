import express from 'express';
import cors from 'cors'; // Import cors module
import productos_routes from './routes/productos.js';

const app = express();
//middlewares
app.use(cors());
app.use(express.json());

app.use(productos_routes);

export default app;