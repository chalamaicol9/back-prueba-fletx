'use strict'
import {Router} from 'express';
const  api = Router();
import * as productoController from '../controllers/productoController.js';


api.get('/producto',productoController.getProducto);
api.post('/producto/crearProducto',productoController.createProducto);
api.put('/producto/actualizarProducto/:id',productoController.updateProducto);
api.delete('/producto/eliminarProducto/:id',productoController.deleteProducto);
api.get('/producto/:id',productoController.getProductoById);
api.post('/producto/comprar', async (req, res) => {
    try {
        const productos = req.body;
        const result = await productoController.comprarProductos(productos);
        res.json(result);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}); 




export default api;