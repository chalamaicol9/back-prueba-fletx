import { Producto } from '../models/producto.js';
import { sequelize } from '../database/connection.js';



const getProducto = (req, res) => {
    Producto.findAll()
        .then((productos) => {
            res.json(productos);
        })
        .catch((error) => {
            console.error('Error al obtener los productos:', error);
            res.status(500).json({ message: 'Hubo un error al procesar la solicitud' });
        });
}

const createProducto = async (req, res) => {

    try {
        const data = req.body;

        const existingProducto = await Producto.findOne({ where: { Nombre: data.Nombre } });
        if (existingProducto) {
            return res.status(400).json({ message: 'Ya existe un producto con este nombre' });
        }
        const producto = await Producto.create(data);
        res.status(201).json({ producto: producto, message: 'Producto creado correctamente' });

    } catch (error) {
        console.error('Error al crear el producto:', error);
        res.status(500).json({ message: 'Hubo un error al procesar la solicitud' });
    }
}

const updateProducto = async (req, res) => {
    try {
        const { id } = req.params;
        const data = req.body;

        const producto = await Producto.findByPk(id);
        if (!producto) {
            return res.status(404).json({ message: 'Producto no encontrado' });
        }

        await producto.update(data);
        res.status(200).send({ data: data, message: 'Producto actualizado correctamente' });

    } catch (error) {
        console.error('Error al actualizar el producto:', error);
        res.status(500).json({ message: 'Hubo un error al procesar la solicitud' });
    }

  
}
const deleteProducto = async (req, res) => {
    try {
        const { id } = req.params;
        const producto = await Producto.findByPk(id);
        if (!producto) {
            return res.status(404).json({ message: 'Producto no encontrado' });
        }
        await producto.destroy();
        res.status(200).json({ message: 'Producto eliminado correctamente' });
    } catch (error) {
        console.error('Error al eliminar el producto:', error);
        res.status(500).json({ message: 'Hubo un error al procesar la solicitud' });
    }
}
const getProductoById = async (req, res) => {
    try {
        const { id } = req.params;
        const producto = await Producto.findByPk(id);
        if (!producto) {
            return res.status(404).json({ message: 'Producto no encontrado' });
        }
        res.json(producto);
    } catch (error) {
        console.error('Error al obtener el producto:', error);
        res.status(500).json({ message: 'Hubo un error al procesar la solicitud' });
    }

}

// crear un servicio que reciba un array de productos estos revisar por id y de acuerdo a la cantidad de productos que teng por id aptualizar la tabla el campo de stock restando la cantidad de productos que se estan comprando
const comprarProductos = async (productos) => {
    const transaction = await sequelize.transaction();
    try {
        // Crear un objeto para llevar el recuento de cada producto
        const productoCounts = productos.reduce((counts, { ID }) => {
            counts[ID] = (counts[ID] || 0) + 1;
            return counts;
        }, {});

        for (const ID in productoCounts) {
            const producto = await Producto.findByPk(ID, { transaction });
            if (!producto) {
                throw new Error(`Producto con ID ${ID} no encontrado`);
            }
            if (producto.Stock < productoCounts[ID]) {
                throw new Error(`No hay suficiente stock del producto con ID ${ID}`);
            }
            producto.Stock -= productoCounts[ID];
            await producto.save({ transaction });
        }
        await transaction.commit();
        return { message: 'Compra realizada con éxito' };
    } catch (error) {
        await transaction.rollback();
        throw error;
    }
}

export { getProducto, createProducto, updateProducto, deleteProducto, getProductoById, comprarProductos};