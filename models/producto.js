// models/producto.js
import { DataTypes } from 'sequelize';
import { sequelize } from '../database/connection.js';
import { Categoria } from './categoria.js';

export const Producto = sequelize.define('Producto', {
  ID: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  Fecha_creacion: {
    type: DataTypes.DATE,
    defaultValue: DataTypes.NOW
  },
  Nombre: {
    type: DataTypes.STRING,
    allowNull: false
  },
  Precio: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  Valor: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  Stock: {
    type: DataTypes.INTEGER,
    allowNull: false
  }
});

Producto.belongsTo(Categoria, { foreignKey: 'CategoriaID', targetKey: 'ID' });
