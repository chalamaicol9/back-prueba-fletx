// models/categoria.js
import { DataTypes } from 'sequelize';
import { sequelize } from '../database/connection.js';

export const Categoria = sequelize.define('Categoria', {
  ID: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  Nombre: {
    type: DataTypes.STRING,
    allowNull: false
  },
  Descripcion: {
    type: DataTypes.TEXT,
    allowNull: true
  }
});
