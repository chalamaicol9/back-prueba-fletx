import app from './app.js';
import { sequelize } from './database/connection.js';
import './models/producto.js';
import './models/categoria.js';
 app.listen(3000,()=>{
     console.log("Servidor corriendo en el puerto 3000");
     sequelize.sync({force:false}).then(()=>{
         console.log("Base de datos sincronizada");
     }).catch((err)=>{
         console.log(err);
     });
 });

